
package tarea.algoritmos.pkg2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *@author Paul Belches
 * @author Mario Sarmientos 
 * @since 30/01/18
 */
public class StackVectorTest {
     /**
     * Test of pop  and push methods, of class StackVector.
     */
    @Test
    public void testPush_Pop() {
        System.out.println("push_pop");
        StackVector instance = new StackVector();
        Object expResult = 12;
        instance.push(expResult);
        Object result = instance.pop();
        assertEquals(expResult, result);
    }

    /**
     * Test of peek method, of class StackVector.
     */
    @Test
    public void testPeek() {
        System.out.println("peek");
        StackVector instance = new StackVector();
        Object expResult = 12;
        instance.push(expResult);
        Object result = instance.peek();
        assertEquals(expResult, result);
    }

    /**
     * Test of size method, of class StackVector.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        StackVector instance = new StackVector();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of empty method, of class StackVector.
     */
    @Test
    public void testEmpty() {
        System.out.println("empty");
        StackVector instance = new StackVector();
        boolean expResult = true;
        boolean result = instance.empty();
        assertEquals(expResult, result);
    }
    
}
