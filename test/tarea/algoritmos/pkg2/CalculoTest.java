
package tarea.algoritmos.pkg2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Paul Belches
 * @author Mario Sarmientos 
 * @since 30/01/18
 */
public class CalculoTest {
    
    public CalculoTest() {
    }

    /**
     * Test of operar method, of class Calculo.
     */
    @Test
    public void testOperar() {
        System.out.println("operar");
        Calculo instance = new Calculo();
        assertEquals(7.0, instance.operar("3 4 +"), 0.0);
        assertEquals(-11.5, instance.operar("3 4 + 5 6 * - 2 /"), 0.0);
        assertEquals(Double.NaN, instance.operar("3 0 /"), 0.0);
        assertEquals(Double.NaN, instance.operar("3 +"), 0.0);
        assertEquals(Double.NaN, instance.operar("a +"), 0.0);
        assertEquals(Double.NaN, instance.operar("a + +"), 0.0);
    }
    
}
