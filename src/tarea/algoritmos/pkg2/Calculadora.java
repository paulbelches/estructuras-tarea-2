package tarea.algoritmos.pkg2;
/**
 *Calculadora.java 
 * Interfaz que tiene los siguientes métodos
 * @author Paul Belches
 * @author Mario Sarmientos 
 * @since 30/01/18
 */
public interface Calculadora { 
    /**
     * Método para operar expresiones postfix.
     * @param expresion en postfix
     * @return el resultado de la operacion
     */
    public double operar(String expresion);
}
